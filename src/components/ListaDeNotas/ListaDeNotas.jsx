import React, {Component} from "react"
import CardNota from "../CardNota/CardNota"
import "./style.css";

export class ListaDeNotas extends Component{

  constructor(props){
    super(props);
  }

      render(){
      return(
        
        <div className="d-flex flex-wrap">
          {this.props.notas.map((nota, index) =>{
            return(
              <div className="card shadow m-2 card-style" key={index}>
                <CardNota indice={index} apagarNota={this.props.apagarNota} titulo={nota.titulo} texto={nota.texto}></CardNota>
              </div>
            )
          })}
        </div>
      )
    }
}

export default ListaDeNotas


