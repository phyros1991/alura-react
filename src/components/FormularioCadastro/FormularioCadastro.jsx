import React, { Component } from "react"

class FormularioCadastro extends Component{
    constructor(props){
        super(props); /* É necessário chamar o super pq ele herda os atributos de seu pai que é o componente */
        this.titulo = "";
        this.text = "";
        this.inputTitulo = React.createRef();
        this.inputText = React.createRef();

    }

    componentDidMount(){
        this.inputTitulo.current.focus();
    }


    _handleMundancaTitulo(evento){
        evento.stopPropagation();
        this.titulo = evento.target.value;
    }

    _handleMudancaTexto(evento){
        evento.stopPropagation();
        this.text = evento.target.value;
    }

    _criarNota(evento){
        evento.preventDefault(); /* Previne o comportamento padrão, evita recarregar a página */
        evento.stopPropagation();

        if(this._verificarInputs()){
            this.props.criarNota(this.titulo, this.text);
            this._limparCampos();
        }
        
    }

    _verificarInputs(){
        if(this.titulo.length < 1 || this.text.length < 1){    
            alert("Campos Vazios, não é possível criar a nota");
            return false;
        }
        return true;
    }

    _limparCampos(){
        this.inputText.current.value = "";
        this.inputTitulo.current.value = "";
        this.titulo = "";
        this.text = "";
    }

    render(){

        return(
            <form className="w-50 mb-5" onSubmit={this._criarNota.bind(this)}>

                <div className="form-group">
                    <label htmlFor="">Titulo</label>
                    <input className="form-control" ref={this.inputTitulo} type="text" placeholder="Título" onChange={this._handleMundancaTitulo.bind(this)}></input>
                </div>

                <div className="form-group">
                    <label htmlFor="">Descrição</label>
                    <textarea className="form-control" ref={this.inputText} rows={15} placeholder="Escreva sua nota" onChange={this._handleMudancaTexto.bind(this)}></textarea>
                </div>

                <div className="d-flex justify-content-center">
                    <button className="btn btn-success">
                        <i className="fas fa-plus fa-2x"></i>
                    </button>
                </div>
            </form>
        )
    }
    
}

export default FormularioCadastro