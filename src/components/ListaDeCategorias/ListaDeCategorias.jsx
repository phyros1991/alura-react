import React, { Component} from 'react';
import "./style.css";

class ListaDeCategorias extends Component{

    _handleEventoInput(e){
        if(e.key == "Enter"){
            console.log("Adicionar Categoria");
        }
    }
    render(){
        return(
            <section>
                <ul>
                    <li className="badge bg-secondary">Categorias</li>
                    <li>Categorias</li>
                    <li>Categorias</li>
                    <li>Categorias</li>
                </ul>

                <div className="d-flex align-items-center">
                    <label className="form-group">Novas Categorias</label>
                    <input 
                        className="form-control mr-2"
                        type="text"
                        onKeyUp={this._handleEventoInput.bind(this)}
                     />
                    <button className="btn btn-success">
                        <i className="fas fa-plus fa-2x"></i>
                    </button>
                </div>
            </section>

        );
    }
}

export default ListaDeCategorias;