import React, { Component } from 'react';
import "./style.css";

class CardNota extends Component {
    constructor(props){
        super(props);
    }


    apagar(){
        const indice = this.props.indice;
        this.props.apagarNota(indice);
    }

    render() { 
        return (  
        
        <section className="card-body card-nota d-flex flex-column justify-content-between">
            <div className="mb-3">
                <header>
                    <h3 className="card-title">{this.props.titulo}</h3>
                </header>
                <div>
                    <p className="card-text">{this.props.texto}</p>
                </div>
            </div>
            <footer>
                <button onClick={this.apagar.bind(this)} className="btn btn-danger">
                    <i className="fas fa-trash fa-1x"></i>
                </button>
            </footer>

        </section>


        );
    }
}
 
export default CardNota;