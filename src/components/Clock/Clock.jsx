import React, { Component } from 'react';

class Clock extends Component{

    constructor(props){
        super(props);
        this.state = {date: new Date()};
    }

    componentDidMount(){
        this.timerID = setInterval(
            () => this.tick(),
            1000);   
    }

    componentWillUnmount() {
        clearInterval(this.timerID);  }


    tick(){
        this.setState({
            date: new Date()
        });
    }


    render(){
        return(
            <div className="d-flex justify-content-center mb-3">
                <div className="w-25">                    
                    <div className="d-flex justify-content-center mb-3">
                        <i className="fas fa-clock fa-4x"></i>
                    </div>

                    <div className="d-flex justify-content-center">
                        <div className="border w-25 mr-2 p-3 bg-info">
                            <h5 className="text-center">{this.state.date.getHours()}</h5>
                        </div>

                        <div className="border w-25 mr-2 p-3 bg-info">
                            <h5 className="text-center">{this.state.date.getMinutes()}</h5>
                        </div>

                        <div className="border w-25 p-3 bg-info">
                            <h5 className="text-center">{this.state.date.getSeconds()}</h5>
                        </div>
                    </div>
                </div>

            </div>
        );
    }

}


export default Clock;