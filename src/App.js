import React, {Component} from "react";
import ListaDeNotas from "./components/ListaDeNotas/index";
import FormularioCadastro from "./components/FormularioCadastro/index";
import './assets/index.css';
import Header from "./components/Header/index";
import Clock from "./components/Clock/index";
import ListaDeCategorias from "./components/ListaDeCategorias/index";

class App extends Component {
    constructor(){
        super()        
        this.state = {
            notas:[],
            notaNumero: 0
        };
        console.log("Estado: " + this.state);
    
    }

    componentDidMount(){
        console.log("Fui renderizado o.o");
    }

    componentDidUpdate(){
        console.log("Fui atualizado :D");
    }

    componentWillUnmount(){
        console.log("Fui desmontado :'/")
    }

    criarNota(titulo, texto){
        const novaNota = {titulo, texto};
        const novoArrayNotas = [...this.state.notas,novaNota];
        const novoEstado = {notas:novoArrayNotas,
                            notaNumero: this.state.notaNumero + 1
                        };

        this.setState(novoEstado);
        console.log("Estado: " + this.state.notas.length);
    }

    deletarNota(index){
        let arrayNotas = this.state.notas;
        arrayNotas.splice(index, 1);
        this.setState({notas:arrayNotas,
                      notaNumero: this.state.notaNumero - 1
                        });
    }


    render() {
        return ( 
            <main className="container">
                <Header></Header>
                <Clock></Clock>

                <div className="d-flex align-items-center flex-column">
                    <i class="far fa-sticky-note fa-4x"></i>
                    <p className="border p-4">{this.state.notaNumero}</p>
                </div>

                <section className="d-flex justify-content-center align-items-center flex-column">
                    <FormularioCadastro criarNota={this.criarNota.bind(this)}> </FormularioCadastro> 
                    <div className="conteudo-principal">
                        <ListaDeCategorias></ListaDeCategorias>
                        <ListaDeNotas apagarNota={this.deletarNota.bind(this)} notas={this.state.notas}> </ListaDeNotas> 
                    </div>
                    
                </section>
            </main>
        );
    }
}

export default App;